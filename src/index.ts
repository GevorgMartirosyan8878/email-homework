import nodemailer from 'nodemailer'
import dotenv from 'dotenv'

dotenv.config();

const user_email = process.env.EMAIL;
const user_password = process.env.PASSWORD;

const transporter = nodemailer.createTransport({
    host: 'smtp.ethereal.email',
    port: 587,
    secure: false,
    auth: {
        user: user_email,
        pass: user_password,
    }
})

const message = {
    from: 'Mailer test <jessika.marquardt56@ethereal.email>',
    to: 'test@test.com',
    title: 'title',
    subject: "Message from node",
    text: 'ինչ որ text',
}

const mailer = (message: object) => {
    transporter.sendMail(message, (err, info) => {
        if (err) console.log(err.message);
        console.log('Email sent: ', info )
    })
}

const SECOND = 1000;
const MINUTE = 60 * SECOND
let count = 0;
const max_message_count = 100;

const sendMail = setInterval(() => {
    mailer(message);
    count++;
    if (count === max_message_count) {
        stoppingSend(sendMail)
    }
}, MINUTE * 5) // MINUTE * 5


function stoppingSend(id: any) {
    clearInterval(id)
}